<?php
// Start the session
session_start();
?>

<!DOCTYPE html>
<html>
  <head>
   <link rel="stylesheet" href="css/estilo.css">
    <link rel="stylesheet" href="./css/bootstrap.min.css" >
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <nav class="navbar navbar-light " id="navbar-nav">
      <a class="navbar-brand" href="menu.php" >
        <img src="./img/Pikachu.png" width="30" height="30" class="d-inline-block align-top" alt="">
       POKEMON
      </a>

      <ul class="nav justify-content-center">
        <li class="nav-item">
          <div class="btn-group">
            <button id="btnReb" class="btn btn-primary btn-lg dropdown-toggle" value="<?php echo $_SESSION['in'];?>"type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
              <?php     if($_SESSION['user'] ){ echo $_SESSION['user'];}else { header('Location: php/logout.php');  }?>
            </button>
            <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
              <a class="dropdown-item" href="perfil.php">Perfil</a>
              <a class="dropdown-item" href="#">HOLA</a>
              <a class="dropdown-item" href="./PHP/logout.php">Cerrar sesion</a>
            </div>
          </div>
        </li>
        <li class="nav-item">
          <a class="nav-link disabled" href="#"></a>
        </li>

        <li class="nav-item">
          <a class="nav-link disabled" href="#"></a>
        </li>
      </ul>
    </nav>

      <div class="container-fluid">
        <div class="row" >
          <div class="col" style="padding:30px" >
            <div class="jumbotron jumbotron-fluid" >
              <div class="container" >
                <div class="row">
                  <div class="col-lg-12">
                      <h1 class="display-3">ENTRENADOR</h1>
                  </div>
                </div>
              <div class="row">
                <div class="col-lg-10">

                      <?php
							require_once './php/conexion.php';
							$nombre = $mysqli->query("SELECT * FROM entrenador WHERE idEntrenador = '".$_SESSION['user']."';");
							if($nombre->num_rows>0){
								while ($row=$nombre->fetch_assoc()) {
									?>

                    <div class="form-group">

                      <input type="hidden" id="sexo" style="visibility:hidden" value="<?php echo $row['generoE']; ?>">
                      <input type="hidden" id="nickname" style="visibility:hidden" value="<?php echo $row['idEntrenador']; ?>">

                      <label for="Nombre" >Nombre</label>
                      <input type="text" id="nombre" value="<?php echo $row['nombreE']; ?>" class="form-control">

                      <label for="Correo" >Correo</label>
                      <input type="text" id="correo" value="<?php echo $row['CorreoE']; ?>" class="form-control">

                      <label for="Contraseña" >Contraseña</label>
                      <input type="password" id="contraseña" value="" class="form-control">

                      <label for="Contraseña" >Repetir Contraseña</label>
                      <input type="password" id="rContraseña" value="" class="form-control">

                      <label for="Genero">Genero</label>
                        <select id="Genero" name="Genero" class="form-control">
                           <option selected>Selecciona tu genero</option>
                           <option >Femenino</option>
                           <option>Masculino</option>
                           <option>Otro</option>

                           <script type="text/javascript">
					    	var genero = document.getElementById("sexo").value;
					    	var buscar = document.getElementById("Genero");
					    	for(var i=0;i<buscar.length;i++){
								if(buscar.options[i].text==genero)
									Genero.selectedIndex = i;
							}
							</script>

                        </select>

                      <div class="trans text-center" style="padding:8px">
                        <button type="submit" class="btn btn-warning"  onclick="Modificar();" name="button">Actualizar</button>
                      </div>

		                         <?php
								}
							}
						?>
                    </div>
                </div>

              </div>
                <div class="row">
                  <div class="col-lg-12">

                  </div>
                </div>

              </div>
            </div>
        </div>


      <script src="./js/jquery.min.js" ></script>
    	<script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" ></script>
      <script src="./js/bootstrap.min.js" ></script>

    	<script type="text/javascript">

		    function Modificar() {
		   	  var nickname = document.getElementById('nickname').value;
		  	  var nombre = document.getElementById('nombre').value;
		      var contra = document.getElementById('contraseña').value;
		      var rcontra = document.getElementById('rContraseña').value;
		      var correo = document.getElementById('correo').value;
		      var aux = document.getElementById('Genero');
		      var genero = aux.options[aux.selectedIndex].text;


		      var parametros = {
		      	'nick' : nickname,
		        'nam': nombre,
		        'con': contra,
		        'gen': genero,
		        'correo': correo,
		        'rcon': rcontra,
		      };

		      $.ajax({
		      	url: "php/modificar.php",
		        type: 'POST',
		        data: parametros,

		        success: function(response){
		          if(response == 'Las contraseñas no coinciden.'){
		            alert(response);
		          }

		          if(response == "Datos actualizados."){
		            alert(response);

		          }
		          console.log(response);
		        }
		      });
		    }
	    </script>


  	</body>
</html>
