-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Servidor: 127.0.0.1
-- Tiempo de generación: 18-12-2017 a las 17:54:08
-- Versión del servidor: 10.1.28-MariaDB
-- Versión de PHP: 7.1.11

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de datos: `laura`
--

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `ataque`
--

CREATE TABLE `ataque` (
  `idAtaque` int(10) UNSIGNED NOT NULL,
  `nombreA` varchar(50) DEFAULT NULL,
  `tipoA` varchar(50) DEFAULT NULL,
  `potenciaA` int(10) UNSIGNED DEFAULT NULL,
  `precisionA` int(10) UNSIGNED DEFAULT NULL,
  `PP` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `ataque`
--

INSERT INTO `ataque` (`idAtaque`, `nombreA`, `tipoA`, `potenciaA`, `precisionA`, `PP`) VALUES
(163, 'Cuchillada', 'normal', 70, 100, 20),
(189, 'Bofetón Lodo', 'ground', 20, 100, 10),
(203, 'Aguante', 'normal', 0, 0, 10),
(430, 'Foco Resplandor', 'steel', 80, 100, 10);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `batalla`
--

CREATE TABLE `batalla` (
  `idB` int(10) UNSIGNED NOT NULL,
  `idEntrenador` varchar(50) NOT NULL,
  `ganador` varchar(50) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `entrenador`
--

CREATE TABLE `entrenador` (
  `idEntrenador` varchar(50) NOT NULL,
  `nombreE` varchar(50) DEFAULT NULL,
  `contraseña` varchar(15) DEFAULT NULL,
  `generoE` varchar(15) DEFAULT NULL,
  `victoriasE` int(10) UNSIGNED DEFAULT NULL,
  `derrotasE` int(10) UNSIGNED DEFAULT NULL,
  `activacion` int(10) UNSIGNED DEFAULT NULL,
  `imagenE` varchar(250) DEFAULT NULL,
  `CorreoE` varchar(140) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `equipo`
--

CREATE TABLE `equipo` (
  `nPokedex` varchar(50) NOT NULL,
  `idEntrenador` varchar(50) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `movimientos`
--

CREATE TABLE `movimientos` (
  `nPokedex` varchar(50) NOT NULL,
  `idAtaque` int(10) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `movimientos`
--

INSERT INTO `movimientos` (`nPokedex`, `idAtaque`) VALUES
('25', 163),
('25', 189),
('25', 203),
('25', 430);

-- --------------------------------------------------------

--
-- Estructura de tabla para la tabla `pokemon`
--

CREATE TABLE `pokemon` (
  `nPokedex` varchar(50) NOT NULL,
  `nombreP` varchar(50) DEFAULT NULL,
  `imagenP` varchar(250) DEFAULT NULL,
  `PS` int(10) UNSIGNED DEFAULT NULL,
  `ataqueP` int(10) UNSIGNED DEFAULT NULL,
  `defensaP` int(10) UNSIGNED DEFAULT NULL,
  `ataqueEspP` int(10) UNSIGNED DEFAULT NULL,
  `defensaEspP` int(10) UNSIGNED DEFAULT NULL,
  `velocidadP` int(10) UNSIGNED DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

--
-- Volcado de datos para la tabla `pokemon`
--

INSERT INTO `pokemon` (`nPokedex`, `nombreP`, `imagenP`, `PS`, `ataqueP`, `defensaP`, `ataqueEspP`, `defensaEspP`, `velocidadP`) VALUES
('25', 'pikachu', 'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/25.png', 35, 55, 40, 50, 50, 90);

--
-- Índices para tablas volcadas
--

--
-- Indices de la tabla `ataque`
--
ALTER TABLE `ataque`
  ADD PRIMARY KEY (`idAtaque`);

--
-- Indices de la tabla `batalla`
--
ALTER TABLE `batalla`
  ADD PRIMARY KEY (`idB`),
  ADD KEY `Batalla_FKIndex1` (`idEntrenador`);

--
-- Indices de la tabla `entrenador`
--
ALTER TABLE `entrenador`
  ADD PRIMARY KEY (`idEntrenador`),
  ADD UNIQUE KEY `CorreoE` (`CorreoE`);

--
-- Indices de la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD PRIMARY KEY (`nPokedex`,`idEntrenador`),
  ADD KEY `Entrenador_has_Pokemon_FKIndex1` (`idEntrenador`),
  ADD KEY `Entrenador_has_Pokemon_FKIndex2` (`nPokedex`);

--
-- Indices de la tabla `movimientos`
--
ALTER TABLE `movimientos`
  ADD PRIMARY KEY (`nPokedex`,`idAtaque`),
  ADD KEY `Pokemon_has_Ataque_FKIndex1` (`nPokedex`),
  ADD KEY `Pokemon_has_Ataque_FKIndex2` (`idAtaque`);

--
-- Indices de la tabla `pokemon`
--
ALTER TABLE `pokemon`
  ADD PRIMARY KEY (`nPokedex`);

--
-- AUTO_INCREMENT de las tablas volcadas
--

--
-- AUTO_INCREMENT de la tabla `batalla`
--
ALTER TABLE `batalla`
  MODIFY `idB` int(10) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- Restricciones para tablas volcadas
--

--
-- Filtros para la tabla `batalla`
--
ALTER TABLE `batalla`
  ADD CONSTRAINT `batalla_ibfk_1` FOREIGN KEY (`idEntrenador`) REFERENCES `entrenador` (`idEntrenador`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `equipo`
--
ALTER TABLE `equipo`
  ADD CONSTRAINT `equipo_ibfk_1` FOREIGN KEY (`idEntrenador`) REFERENCES `entrenador` (`idEntrenador`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `equipo_ibfk_2` FOREIGN KEY (`nPokedex`) REFERENCES `pokemon` (`nPokedex`) ON DELETE NO ACTION ON UPDATE NO ACTION;

--
-- Filtros para la tabla `movimientos`
--
ALTER TABLE `movimientos`
  ADD CONSTRAINT `movimientos_ibfk_1` FOREIGN KEY (`nPokedex`) REFERENCES `pokemon` (`nPokedex`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  ADD CONSTRAINT `movimientos_ibfk_2` FOREIGN KEY (`idAtaque`) REFERENCES `ataque` (`idAtaque`) ON DELETE NO ACTION ON UPDATE NO ACTION;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
