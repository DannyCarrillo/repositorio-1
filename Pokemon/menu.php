<?php
  require_once 'php/conexion.php';
  session_start();
?>
<!DOCTYPE html>
<html>
  <head>
    <link rel="stylesheet" href="css/estilo.css">
    <link rel="stylesheet" href="./css/bootstrap.min.css" >
    <meta charset="utf-8">
    <title></title>
  </head>
  <body>
    <nav class="navbar navbar-light " id="navbar-nav">
      <a class="navbar-brand" href="index.html" >
        <img src="./img/Pikachu.png" width="30" height="30" class="d-inline-block align-top" alt="">
       POKEMON
      </a>

      <div class="dropdown">
      <button class="btn btn-warning  dropdown-toggle" type="button" id="dropdownMenuButton" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
        <?php
        $name =  $_SESSION['user'];
        if($_SESSION['user'] && $name != ""){
          echo $name;
        }else {
          header('Location: index.html');
        } ?>
      </button>
      <div class="dropdown-menu" aria-labelledby="dropdownMenuButton">
        <a class="dropdown-item" href="perfil.php">Ver perfil</a>
        <a class="dropdown-item" href="./php/logout.php">Cerrar sesión</a>
      </div>

      </div>
    </nav>
    <div class="container-fluid pt-5">
      <div class="row">
        <div class="col-lg-3">
          <div class="list-group">
            <li class="list-group-item list-group-item-warning">
              <div class="d-flex w-100 justify-content-between">
                <h3 class="mb-1"><?php echo $_SESSION['user'];?></h3>
                <?php
                  $result = $mysqli->query("select * from entrenador where idEntrenador = '".$_SESSION['user']."'");
                  if($result->num_rows > 0){
                    while ($row = $result->fetch_assoc()) {
                      ?> <img src="<?php echo $row['imagenE'];?>" width="70" height="125" alt="">  <?php
                    }
                    $result->close();
                  }

                ?>
              </div>
            </li>
            <a href="perfil.php" class="list-group-item list-group-item-action flex-column align-items-start">
              <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">Ir al perfil</h5>
              </div>
              <p class="mb-1">En el perfil puedes consultar tus datos y actualizarlos.</p>

            </a>
            <a href="Artificial.html" class="list-group-item list-group-item-action flex-column align-items-start">
              <div class="d-flex w-100 justify-content-between">
                <h5 class="mb-1">Crear entrenador artificial </h5>
              </div>
              <p class="mb-1">Cree entrenador artificial para empezar una batalla pokemon</p>
              </a>
              <a href="batalla.php" class="list-group-item list-group-item-action flex-column align-items-start">
                <div class="d-flex w-100 justify-content-between">
                  <h5 class="mb-1">Batallas Pokemon</h5>
                </div>
                <p class="mb-1">demuestra tus habilidades como entrenador pokemon </p>
                </a>
          </div>
        </div>
        <div class="col-lg-9 bg-light rounded">
          <br>
          <div class="row">
            <div class="col-lg-12">
              <div class="row">
                <div class="col-lg-12">
                  <h4 class=" text-center mb-1"> Equipo pokemon</h4>
                </div>
              </div>
              <div class="row">
                <div class="col-lg-12">
                  <div class="card-group">
                  <?php
                  $cont = 0;
                  $pivote = 1;
                    $buscaPokes = $mysqli->query("select * from equipo where idEntrenador ='".$_SESSION['user']."'");
                    if($buscaPokes->num_rows > 0){
                      while ($row = $buscaPokes->fetch_assoc()) {
                        $buscaEntradaPoke = $mysqli->query("select * from pokemon where nPokedex ='".$row['nPokedex']."'");
                        if($buscaEntradaPoke->num_rows > 0){
                          while ($rowpokemons = $buscaEntradaPoke->fetch_assoc()) {
                            if (($cont % 2) == 0)
                            {?>

                                  <?php
                            }
                            if($cont == 2*$cont){
                              ?><?php
                            }
                            ?><div class="card" style="width: 20rem;">
                                <img class="card-img-top" src="<?php echo  $rowpokemons['imagenP']; ?>" alt="Card image cap">
                                <div class="card-body">
                                  <h4 class="card-title"><?php echo $rowpokemons['nombreP']; ?></h4>
                                  <p class="text-center card-text"><strong>Caracteristicas:</strong></p>
                                  <div class="row">
                                    <div class="col-lg-6">
                                      <p>Puntos de salud: <?php echo $rowpokemons['PS']; ?> </p>
                                    </div>
                                    <div class="col-lg-6">
                                      <p>Ataque: <?php echo $rowpokemons['ataqueP']; ?> </p>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-lg-6">
                                      <p>Ataque especial: <?php echo $rowpokemons['ataqueEspP']; ?> </p>
                                    </div>
                                    <div class="col-lg-6">
                                      <p>Defensa: <?php echo $rowpokemons['defensaP']; ?> </p>
                                    </div>
                                  </div>
                                  <div class="row">
                                    <div class="col-lg-6">
                                      <p>Defensa especial: <?php echo $rowpokemons['defensaEspP']; ?> </p>
                                    </div>
                                    <div class="col-lg-6">
                                      <p>Velocidad: <?php echo $rowpokemons['velocidadP']; ?> </p>
                                    </div>
                                  </div>

                                </div>
                                <ul class="list-group list-group-flush">
                                  <?php
                                  $buscaAtaquePoke = $mysqli->query("select * from movimientos where nPokedex ='".$row['nPokedex']."'");
                                  if($buscaAtaquePoke->num_rows > 0){
                                    while ($row1 = $buscaAtaquePoke->fetch_assoc()) {
                                      $ataque = $mysqli->query("select * from ataque where idAtaque ='".$row1['idAtaque']."'");
                                      if($ataque->num_rows > 0){
                                        while ($row2 = $ataque->fetch_assoc()) {
                                          ?><li class="list-group-item"><?php echo $row2['nombreA'] ?> <span class="badge badge-warning">Poder: <?php echo $row2['potenciaA'];?></span> </li><?php
                                        }
                                      }
                                    }
                                  }
                                 ?>
                                </ul>
                              </div><?php
                              if($cont == $pivote){
                                $pivote += 2;
                                ?><?php
                              }
                              if(($cont % 2) == 0){
                                ?>
                              <?php
                              }
                              $cont++;
                            }
                          }
                        }
                        ?></div>
                      </div>
                    </div><?php
                      }
                    ?>

            </div>
          </div>

        </div>
      </div>

    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" ></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.12.3/umd/popper.min.js" ></script>
    <script src="./js/bootstrap.min.js" ></script>
  </body>
  </html>
