<?php
require_once 'conexion.php';
$idEntr = $_REQUEST["ide"];
$nombre = $_REQUEST["nom"];
$nivel = $_REQUEST["niv"];
$pokemon1 = $_REQUEST['po1'];
$pokemon2 = $_REQUEST['po2'];
$pokemon3 = $_REQUEST['po3'];
$pokemon4 = $_REQUEST['po4'];
$pokemon5 = $_REQUEST['po5'];
$pokemon6 = $_REQUEST['po6'];
$arr_pokemons = [$pokemon1,$pokemon2,$pokemon3,$pokemon4,$pokemon5,$pokemon6];
echo $idEntr." - ";
$resultado =  $mysqli->query("Select * from entrenadora WHERE idEntrenadorA = '$idEntr'");
if ($resultado->num_rows <= 0 && $idEntr != "") {
    $mysqli->query("insert into entrenadora values('$idEntr','$nombre','$nivel')");
    $cadena = "http://pokeapi.co/api/v2/pokemon/";
    $cadena_ataques = 'http://pokeapi.co/api/v2/move/';
    $llegada = 0;
    $idioma ="es";
    while ($llegada < 6) {
      $array_entradas_pokedex = array();
      $tipos_stats_necesarios = ["speed","special-defense","special-attack","defense","attack","hp"];
      $array_stats = array();
      $stats = array();
      $array_tipos = array();
      $texto_tipos = "";
      $id = 1;
      $idPoke =  $arr_pokemons[$llegada];
      $data = file_get_contents($cadena.$idPoke.'/');
      if($data != ""){
          $pok = json_decode($data);
          $nombre_pokemon = $pok->name;
          $imagen_pokemon = $pok->sprites->front_default;
          $array_tipos = $pok->types;
          $array_stats = $pok->stats;

          for ($i=0; $i < count($tipos_stats_necesarios); $i++) {
            foreach($array_stats as $obj){
              if ($tipos_stats_necesarios[$i] == $obj->stat->name) {
                $stats[] = $obj->base_stat;
              }
            }
          }
          foreach($array_tipos as $obj){
            $texto_tipos .= $obj->type->name . ",";
          }
          $url_especie = $pok->species->url;
          $info_especie = file_get_contents($url_especie);
          if($info_especie != ""){
            $datos_pokedex = json_decode($info_especie);
            $entradas_en_diferentes_id = $datos_pokedex->flavor_text_entries;
            foreach ($entradas_en_diferentes_id as $obj) {
              if($idioma == $obj->language->name){
                $array_entradas_pokedex = $obj->flavor_text;
              }
            }
          }

          $busquedaP = $mysqli->query("select * from pokemon where nPokedex = '".$idPoke."'");
          if($busquedaP->num_rows  <= 0){
            $mysqli->query("insert into pokemon values('$idPoke','$nombre_pokemon','$imagen_pokemon','$stats[5]','$stats[4]','$stats[3]','$stats[2]','$stats[1]','$stats[0]')");
            $mysqli->query("insert into equipoa values('$idEntr','$idPoke')");
          }else {
              $mysqli->query("insert into equipoa values('$idEntr','$idPoke')");
          }
          unset($stats);
          $busquedaP->close();

          for ($i=0; $i < 4; $i++) {
            $id_move =  mt_rand(1,719);
            $data_moves = file_get_contents($cadena_ataques.$id_move.'/');
            if($data_moves != ""){
              $movimientos = json_decode($data_moves);
              $nombre_ataque = "";
              $puntosp = $movimientos->pp;
              $pres = $movimientos->accuracy;
              $poder = $movimientos->power;
              $tipo_movimiento = $movimientos->type->name;
              $entradas_en_diferentes_idiomas = $movimientos->names;
              foreach ($entradas_en_diferentes_idiomas as $obj) {
                if($idioma == $obj->language->name){
                  $id_move = $obj->name;
                }
              }
              $mysqli->query("insert into ataque values('$id_move','$nombre_ataque','$tipo_movimiento','$poder','$pres','$puntosp')");
              $mysqli->query("insert into movimientos values('$idPoke',$id_move')");
            }
          }
        $llegada++;
      }
    }
    $mysqli->close();
    echo 'registro exitoso.';
}else{
  echo 'El entrenador artificial con ese nick ya existe o no puede ser nulo.';
  $resultado->close();
  $mysqli->close();

}
 ?>
